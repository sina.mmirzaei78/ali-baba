package com.krdbootcamp.user.user;

import com.krdbootcamp.user.common.BaseEntity;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = User.USER_TB_NAME)
@Data
@Audited
public class User extends BaseEntity {
    public static final String USER_TB_NAME = "tbl_user";

    private String name;

    private String nationalCode;

    private String phoneNumber;
}
