package com.krdbootcamp.user.user;

import com.krdbootcamp.user.common.exception.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Override
    public User save(User user) {
        return repository.save(user);
    }

    @Override
    public User update(User user) {
        User savedBefore = getById(user.getId());
        savedBefore.setName(user.getName());
        savedBefore.setPhoneNumber(user.getPhoneNumber());
        savedBefore.setNationalCode(user.getNationalCode());

        return repository.save(savedBefore);
    }

    @Override
    public void delete(Long userId) {
        repository.delete(getById(userId));
    }

    @Override
    public User getById(Long userId) {
        Optional<User> optionalUser = repository.findById(userId);
        if (optionalUser.isEmpty()) throw new NotFoundException("User Not Found");
        return optionalUser.get();
    }
}
