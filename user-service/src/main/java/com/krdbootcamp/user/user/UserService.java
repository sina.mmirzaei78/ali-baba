package com.krdbootcamp.user.user;

public interface UserService {

    User save(User user);

    User update(User user);

    void delete(Long userId);

    User getById(Long userId);
}