package com.krdbootcamp.user.user;

import com.krdbootcamp.user.common.BaseDTO;
import com.krdbootcamp.user.common.BaseEntity;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
public class UserDTO extends BaseDTO {
    private String name;

    private String nationalCode;

    private String phoneNumber;
}
