package com.krdbootcamp.user.common.exception;
public class FileSavingException extends RuntimeException {
    public FileSavingException(String message) {
        super(message);
    }
}
