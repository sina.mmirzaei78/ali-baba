package com.krdbootcam.gateway;


import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudConfig {

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/userservice/**")
                        .uri("lb://user-service")
                )
                .route(r -> r.path("/location/**")
                        .uri("lb://user-address-service")
                )
                .build();
    }

}