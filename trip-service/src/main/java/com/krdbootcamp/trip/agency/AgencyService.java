package com.krdbootcamp.trip.agency;


import com.krdbootcamp.trip.trip.Trip;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AgencyService implements IAgencyService {
    private  final AgencyRepository repository;

    @Override
    public Agency save(Agency agency) {

        return repository.save(agency);
    }

    @Override
    public Agency update(Agency agency) {

        Agency lastAgency = getById(agency.getId());

        lastAgency.setName(agency.getName());
        lastAgency.setPhoneNumber(agency.getPhoneNumber());

        return repository.save(lastAgency);
    }

    @Override
    public void delete(Long id) {

        getById(id);
        repository.deleteById(id);
    }

    @Override
    public Agency getById(Long id) {

        Optional<Agency> optionalAgency=repository.findById(id);

        if (!optionalAgency.isPresent()){

            throw new NotFoundException("Agency Not Found");
        }

        return optionalAgency.get();
    }

    @Override
    public List<Agency> getAll() {

        return (List<Agency>) repository.findAll();
    }
}
