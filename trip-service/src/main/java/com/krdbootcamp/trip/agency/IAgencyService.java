package com.krdbootcamp.trip.agency;


import java.util.List;

public interface IAgencyService {

    Agency save(Agency agency);
    Agency update(Agency agency);
    void delete(Long id);
    Agency getById(Long id);
    List<Agency> getAll();
}
