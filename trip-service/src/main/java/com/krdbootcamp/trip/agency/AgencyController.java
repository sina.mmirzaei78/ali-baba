package com.krdbootcamp.trip.agency;


import com.krdbootcamp.trip.trip.ITripService;
import com.krdbootcamp.trip.trip.Trip;
import com.krdbootcamp.trip.trip.TripDTO;
import com.krdbootcamp.trip.trip.TripMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/agency/")
@AllArgsConstructor
public class AgencyController {


    private final IAgencyService agencyService;
    private AgencyMapper agencyMapper;

    @PostMapping("/v1")
    public ResponseEntity save(@RequestBody AgencyDTO agencyDTO){
        Agency agency =agencyMapper.toAgency(agencyDTO);
        agencyService.save(agency);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/v1")
    public ResponseEntity update(@RequestBody AgencyDTO agencyDTO){
        Agency agency =agencyMapper.toAgency(agencyDTO);
        agencyService.update(agency);
        return ResponseEntity.ok().build();
    }


    @GetMapping("/v1/{id}")
    public ResponseEntity<AgencyDTO> getById(@PathVariable Long id ){
        Agency agency = agencyService.getById(id);
        AgencyDTO agencyDTO =agencyMapper.toAgencyDTO(agency);
        return ResponseEntity.ok(agencyDTO);
    }


    @GetMapping("/v1")
    public ResponseEntity<List<AgencyDTO>> getAll(){
        List<Agency> agencies= agencyService.getAll();
        List<AgencyDTO> agencyDTOS = agencyMapper.toAgencyDTOs(agencies);

        return ResponseEntity.ok(agencyDTOS);
    }

    @DeleteMapping("/v1/{id}")
    public ResponseEntity delete(@PathVariable Long id){

        agencyService.delete(id);
        return ResponseEntity.ok().build();

    }
}
