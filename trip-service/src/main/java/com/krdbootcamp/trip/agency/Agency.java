package com.krdbootcamp.trip.agency;


import com.krdbootcamp.trip.common.BaseEntity;
import com.krdbootcamp.trip.trip.Trip;
import com.krdbootcamp.trip.trip.Type;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "tbl_agency")
@Data
@Audited
public class Agency extends BaseEntity {

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "phone_number")
    private String phoneNumber;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "agency",cascade = CascadeType.ALL)
    private List<Trip> trips;

}
