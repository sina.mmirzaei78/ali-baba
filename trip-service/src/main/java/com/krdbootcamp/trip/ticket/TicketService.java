package com.krdbootcamp.trip.ticket;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TicketService implements ITicketService{

    private final TicketRepository ticketRepository;

    @Override

    public Ticket save(Ticket ticket)
    {
        return ticketRepository.save(ticket);
    }

    @Override
    public Ticket update(Ticket ticket) {
        Ticket lastticket = getById(ticket.getId());

        lastticket.setCode(ticket.getCode());
        lastticket.setStatus(ticket.getStatus());
        lastticket.setPaid_price(ticket.getPaid_price());

        return ticketRepository.save(lastticket);


    }


    @Override

    public void delete(Long id) {
        getById(id);
        ticketRepository.deleteById(id);
    }

    @Override

    public Ticket getById(Long id) {
        Optional<Ticket> optionalTicket = ticketRepository.findById(id);
        if (optionalTicket.isEmpty()) {
            throw new NotFoundException("Ticket Not Found!");
        }
        return optionalTicket.get();
    }

    @Override
    public List<Ticket> getAll() {
        return null;
    }


    @Override
    public List<Ticket> findAll() {

        return (List<Ticket>) ticketRepository.findAll();
    }




}
