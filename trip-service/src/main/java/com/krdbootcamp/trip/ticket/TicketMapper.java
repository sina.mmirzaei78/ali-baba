package com.krdbootcamp.trip.ticket;

import com.krdbootcamp.trip.trip.TripMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = TripMapper.class)
public interface TicketMapper {


    Ticket toTicket(TicketDTO ticketDTO);
    TicketDTO toTicketDTO(Ticket ticket);
    List<Ticket> toTicketList(List<TicketDTO> ticketDTOS);
    List<TicketDTO> toTicketDTOList(List<Ticket> tickets);
}
