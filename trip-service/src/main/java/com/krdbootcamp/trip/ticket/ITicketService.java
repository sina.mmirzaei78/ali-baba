package com.krdbootcamp.trip.ticket;

import java.util.List;

public interface ITicketService {

    Ticket save(Ticket ticket);

    Ticket update(Ticket ticket);

    void delete(Long id);

    Ticket getById(Long id);

    List<Ticket> getAll();

    List<Ticket> findAll();
}
