package com.krdbootcamp.trip.ticket;

import com.krdbootcamp.trip.trip.Trip;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends PagingAndSortingRepository<Ticket,Long> {


    List<Ticket> findAll(Specification<Ticket> specification);


    List<Ticket> findAllByTrip(Trip trip);
}
