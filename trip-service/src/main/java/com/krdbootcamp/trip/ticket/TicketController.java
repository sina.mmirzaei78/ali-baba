package com.krdbootcamp.trip.ticket;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@AllArgsConstructor
@RequestMapping("/ticket")
public class TicketController {

    private final ITicketService ticketService;
    private TicketMapper mapper;

    @PostMapping("/v1")
    public ResponseEntity<?> save(@RequestBody TicketDTO ticketDTO) {
        Ticket ticket = mapper.toTicket(ticketDTO);
        ticketService.save(ticket);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/v1")
    public ResponseEntity<?> update(@RequestBody TicketDTO ticketDTO) {
        Ticket ticket = mapper.toTicket(ticketDTO);
        ticketService.update(ticket);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/v1/{id}")
    public ResponseEntity<TicketDTO> getById(@PathVariable Long id) {
        Ticket ticket = ticketService.getById(id);
        TicketDTO ticketDTO = mapper.toTicketDTO(ticket);
        return ResponseEntity.ok(ticketDTO);
    }

    @DeleteMapping("/v1/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        ticketService.delete(id);
        return ResponseEntity.ok().build();
    }









}
