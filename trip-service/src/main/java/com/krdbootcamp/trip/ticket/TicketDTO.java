package com.krdbootcamp.trip.ticket;

import com.krdbootcamp.trip.common.BaseDTO;
import com.krdbootcamp.trip.trip.Trip;
import com.krdbootcamp.trip.trip.TripDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class TicketDTO extends BaseDTO {

    @ApiModelProperty(required = true, hidden = false)
    private String name;

    @ApiModelProperty(required = true, hidden = false)
    private String price;

    @ApiModelProperty(required = true, hidden = false)
    private String image;


    @ApiModelProperty(required = true, hidden = false)
    private TripDTO trip;
}
