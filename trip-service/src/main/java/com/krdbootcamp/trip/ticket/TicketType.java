package com.krdbootcamp.trip.ticket;

public enum TicketType {
    AIRPLANE,
    BUS,
    RIDING,
    TRAIN,
    SHIP

}
