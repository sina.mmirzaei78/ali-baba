package com.krdbootcamp.trip.ticket;

import com.krdbootcamp.trip.common.BaseDTO;
import com.krdbootcamp.trip.trip.Trip;
import lombok.Data;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Point;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "tbl-ticket")
@Audited
@Data
public class Ticket extends BaseDTO {

    @Column(name = "code")
    private  int code;

    @Column(name = "status")
    private  String status;

    @Column(name = "paid_price")
    private  String paid_price;



    @ManyToOne()
    @JoinColumn(name = "trip_id")
    private Trip trip;


    @NotNull
    @Column(name = "mossafer_id")
    private Long mossaferId;
}
