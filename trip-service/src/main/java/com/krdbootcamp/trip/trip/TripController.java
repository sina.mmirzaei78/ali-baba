package com.krdbootcamp.trip.trip;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/trip/")
@AllArgsConstructor
public class TripController {


    private final ITripService tripService;
    private TripMapper tripMapper;

    @PostMapping("/v1")
    public ResponseEntity save(@RequestBody TripDTO tripDTO){
        Trip trip =tripMapper.toTrip(tripDTO);
        tripService.save(trip);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/v1")
    public ResponseEntity update(@RequestBody TripDTO tripDTO){
        Trip trip =tripMapper.toTrip(tripDTO);
        tripService.update(trip);
        return ResponseEntity.ok().build();
    }


    @GetMapping("/v1/{id}")
    public ResponseEntity<TripDTO> getById(@PathVariable Long id ){
        Trip trip = tripService.getById(id);
        TripDTO tripDTO =tripMapper.toTripDTO(trip);
        return ResponseEntity.ok(tripDTO);
    }


    @GetMapping("/v1")
    public ResponseEntity<List<TripDTO>> getAll(){
        List<Trip> trips= tripService.getAll();
        List<TripDTO> tripDTOS = tripMapper.toTripDTOs(trips);

        return ResponseEntity.ok(tripDTOS);
    }

    @DeleteMapping("/v1/{id}")
    public ResponseEntity delete(@PathVariable Long id){

        tripService.delete(id);
        return ResponseEntity.ok().build();

    }


    @GetMapping("/v1/get-all-by-agency/{agencyId}")
    public ResponseEntity<List<TripDTO>> getAllByAgency(@PathVariable Long agencyId){
        List<Trip> trips= tripService.getAllByAgency(agencyId);
        List<TripDTO> tripDTOS = tripMapper.toTripDTOs(trips);

        return ResponseEntity.ok(tripDTOS);
    }
}
