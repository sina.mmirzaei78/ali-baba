package com.krdbootcamp.trip.trip;

import com.krdbootcamp.trip.agency.AgencyMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AgencyMapper.class})
public interface TripMapper {

    Trip toTrip(TripDTO tripDTO);

    TripDTO toTripDTO(Trip trip);

    List<Trip> toTripList(List<TripDTO> tripDTOS);
    List<TripDTO> toTripDTOs(List<Trip> trips);


}
