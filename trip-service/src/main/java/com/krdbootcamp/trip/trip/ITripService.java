package com.krdbootcamp.trip.trip;

import com.krdbootcamp.trip.agency.Agency;

import java.util.List;

public interface ITripService {

    Trip save(Trip trip);
    Trip update(Trip trip);
    void delete(Long id);
    Trip getById(Long id);
    List<Trip> getAll();

    List<Trip> getAllByAgency(Long agencyId);
}
