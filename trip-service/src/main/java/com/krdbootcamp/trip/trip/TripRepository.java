package com.krdbootcamp.trip.trip;

import com.krdbootcamp.trip.agency.Agency;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TripRepository extends PagingAndSortingRepository<Trip, Long> {

    List<Trip> findAllByAgency(Agency agency);

}
