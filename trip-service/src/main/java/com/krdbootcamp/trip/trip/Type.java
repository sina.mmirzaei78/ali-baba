package com.krdbootcamp.trip.trip;

public enum Type {

    DOMESTICFLIGHT,
    INTERNATIONALFLIGHT,
    TRAIN,
    BUS,
    TOUR
}
