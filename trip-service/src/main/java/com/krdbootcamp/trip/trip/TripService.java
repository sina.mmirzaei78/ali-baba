package com.krdbootcamp.trip.trip;


import com.krdbootcamp.trip.agency.Agency;
import com.krdbootcamp.trip.agency.AgencyService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TripService implements ITripService{
    private  final TripRepository repository;

    private final AgencyService agencyService;
    @Override
    public Trip save(Trip trip) {

        return repository.save(trip);
    }

    @Override
    public Trip update(Trip trip) {
        Trip lastTrip = getById(trip.getId());

        lastTrip.setPrice(trip.getPrice());
        lastTrip.setCount(trip.getCount());
        lastTrip.setCityFrom(trip.getCityFrom());
        lastTrip.setCityTo(trip.getCityTo());
        lastTrip.setEndTime(trip.getEndTime());
        lastTrip.setStartTime(trip.getStartTime());
        lastTrip.setType(trip.getType());

        return repository.save(lastTrip);
    }

    @Override
    public void delete(Long id) {

        getById(id);
        repository.deleteById(id);
    }

    @Override
    public Trip getById(Long id) {
        Optional<Trip> optionalTrip=repository.findById(id);

        if (!optionalTrip.isPresent()){

            throw new NotFoundException("Trip Not Found");
        }

        return optionalTrip.get();
    }

    @Override
    public List<Trip> getAll() {
        return (List<Trip>) repository.findAll();
    }

    @Override
    public List<Trip> getAllByAgency(Long agencyId) {

        Agency agency = agencyService.getById(agencyId);
        return repository.findAllByAgency(agency);
    }
}
