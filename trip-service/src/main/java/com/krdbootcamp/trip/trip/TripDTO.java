package com.krdbootcamp.trip.trip;


import com.krdbootcamp.trip.agency.Agency;
import com.krdbootcamp.trip.agency.AgencyDTO;
import com.krdbootcamp.trip.common.BaseDTO;
import com.krdbootcamp.trip.common.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class TripDTO extends BaseDTO {

    @ApiModelProperty(required = true,hidden = false)
    private Double price;

    @ApiModelProperty(required = true,hidden = false)
    private Type type;

    @ApiModelProperty(required = true,hidden = false)
    private Long startTime;

    @ApiModelProperty(required = true,hidden = false)
    private Long endTime;

    @ApiModelProperty(required = true,hidden = false)
    private Integer count;

    @ApiModelProperty(required = true,hidden = false)
    private String cityFrom;

    @ApiModelProperty(required = true,hidden = false)
    private String cityTo;

    @ApiModelProperty(required = true,hidden = false)
    private AgencyDTO agency;


}
