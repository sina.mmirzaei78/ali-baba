package com.krdbootcamp.trip.trip;


import com.krdbootcamp.trip.agency.Agency;
import com.krdbootcamp.trip.common.BaseEntity;
import com.krdbootcamp.trip.ticket.Ticket;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "tbl_trip")
@Data
@Audited
public class Trip extends BaseEntity {

    @NotNull
    @Column(name = "price")
    private Double price;

    @NotNull
    @Column(name = "type")
    private Type type;

    @NotNull
    @Column(name = "start_time")
    private Long startTime;

    @NotNull
    @Column(name = "end_time")
    private Long endTime;

    @NotNull
    @Column(name = "count")
    private Integer count;

    @NotNull
    @Column(name = "city_from")
    private String cityFrom;

    @NotNull
    @Column(name = "city_to")
    private String cityTo;

    @ManyToOne()
    @JoinColumn(name = "agency_id")
    private Agency agency;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "trip",cascade = CascadeType.ALL)
    private List<Ticket> tickets;

}
