package com.krdbootcam.location.province;


import com.krdbootcam.location.common.BaseEntity;
import com.krdbootcam.location.country.Country;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Audited
@Table(name= "tbl_provie")
@Data
public class Province extends BaseEntity {

    @NotNull
    @Column(name = "name")
    private  String name;

   @ManyToOne
   @JoinColumn(name = "country_id")
    private Country country;
}
