package com.krdbootcam.location.province;

import com.krdbootcam.location.common.PagingData;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/api-province-controller")
@AllArgsConstructor
public class ProvinceController {

    private final ProvinceMapper mapper;
    private final IProvinceService iProvinceService;


    @PostMapping
    public ResponseEntity<ProvinceDTO> save(@RequestBody ProvinceDTO provinceDTO){
        Province province=mapper.toProvince(provinceDTO);
        iProvinceService.save(province);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping
    public ResponseEntity delete (@PathVariable Long id){
        iProvinceService.delete(id);
        return ResponseEntity.ok().build();

    }

    @GetMapping
    public ResponseEntity<ProvinceDTO> findById(@PathVariable Long id){
        Province province=iProvinceService.findById(id);
        ProvinceDTO provinceDTO=mapper.toProvinceDTO(province);
        return ResponseEntity.ok(provinceDTO);
    }

    @PutMapping
    public ResponseEntity<ProvinceDTO> update(@RequestBody ProvinceDTO provinceDTO){
      Province province=mapper.toProvince(provinceDTO);
      iProvinceService.update(province);
      return ResponseEntity.ok(provinceDTO);
    }

    @GetMapping("/{page}/{size}")
    public ResponseEntity<PagingData<ProvinceDTO>> findAll(@PathVariable Integer page, @PathVariable Integer size){
        Page<Province> provincePage=iProvinceService.findAll(page,size);
        int total=provincePage.getTotalPages();
        List<Province> provinces=provincePage.getContent();
        List<ProvinceDTO> provinceDTOS=mapper.toProvinceDTOs(provinces);
        PagingData<ProvinceDTO> provinceDTOPagingData=new PagingData<>(total,page,provinceDTOS);
        return ResponseEntity.ok(provinceDTOPagingData);
    }


    @GetMapping("/all-by-country-id/{page}/{size}/{countryId}")
    public ResponseEntity<PagingData<ProvinceDTO>> getAllByCountryId(@PathVariable Integer page, @PathVariable Integer size,@PathVariable Long countryId){
        Page<Province> provincePage=iProvinceService.findByCountry(page,size, countryId);
        int total=provincePage.getTotalPages();
        List<Province> provinces=provincePage.getContent();
        List<ProvinceDTO> provinceDTOS=mapper.toProvinceDTOs(provinces);
        PagingData<ProvinceDTO> provinceDTOPagingData=new PagingData<>(total,page,provinceDTOS);
        return ResponseEntity.ok(provinceDTOPagingData);
    }






}
