package com.krdbootcam.location.province;

import com.krdbootcam.location.common.BaseDTO;
import com.krdbootcam.location.country.Country;
import com.krdbootcam.location.country.CountryDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProvinceDTO extends BaseDTO {

    @ApiModelProperty(required = true,hidden = false)
    private  String name;

    @ApiModelProperty(required = true,hidden = false)
    private CountryDTO country;
}
