package com.krdbootcam.location.province;

import com.krdbootcam.location.country.CountryMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring", uses = {CountryMapper.class})
public interface ProvinceMapper {

    Province toProvince(ProvinceDTO provinceDTO);
    ProvinceDTO toProvinceDTO(Province province);
    List<Province> toProvinceList(List<ProvinceDTO> provinceDTOS);
    List<ProvinceDTO> toProvinceDTOs(List<Province> provinceList);

}
