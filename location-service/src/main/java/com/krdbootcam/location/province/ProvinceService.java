package com.krdbootcam.location.province;

import com.krdbootcam.location.common.exception.NotFoundException;
import com.krdbootcam.location.country.Country;
import com.krdbootcam.location.country.ICountryService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class ProvinceService implements IProvinceService {

    private final ProvinceRepository repository;
    private final ICountryService iCountryService;

    @Override
    public Province save(Province province) {
        Long countryId= province.getCountry().getId();
        Country country=iCountryService.findById(countryId);
        province.setCountry(country);
        return repository.save(province);
    }

    @Override
    public Province update(Province province) {
        Province lastProvinceSave=findById(province.getId());
        lastProvinceSave.setName(province.getName());
        Long countryId= province.getCountry().getId();
        Country country=iCountryService.findById(countryId);
        province.setCountry(country);
        return repository.save(lastProvinceSave);
    }

    @Override
    public void delete(Long id) {
        findById(id);
        repository.deleteById(id);

    }
    @Override
    public Province findById(Long id) {
        Optional<Province> optionalBasketItem=repository.findById(id);
        if (!optionalBasketItem.isPresent()){
            throw  new NotFoundException("Not Found Basket");
        }
        return optionalBasketItem.get();
    }

    @Override
    public Page<Province> findByCountry(Integer page, Integer size, Long countryId) {
        return repository.findAllByCountry_Id(PageRequest.of(page,size),countryId);
    }

    @Override
    public Page<Province> findAll(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page,size));
    }




}
