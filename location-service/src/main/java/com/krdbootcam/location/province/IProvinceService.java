package com.krdbootcam.location.province;

import org.springframework.data.domain.Page;


public interface IProvinceService {

    Province save(Province province);
    Province update(Province province);
    void delete(Long id);
    Province findById(Long id);
    Page<Province> findByCountry(Integer page, Integer size,Long countryId);
    Page<Province> findAll(Integer page, Integer size);

}
