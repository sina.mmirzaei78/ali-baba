package com.krdbootcam.location.province;

import com.krdbootcam.location.country.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProvinceRepository extends PagingAndSortingRepository<Province,Long> {

    Page<Province> findAllByCountry_Id(Pageable pageable, Long countryId);
    Page<Province>findAll(Pageable pageable);

}
