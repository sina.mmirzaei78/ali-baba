package com.krdbootcam.location.country;

import com.krdbootcam.location.common.exception.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CountryService implements ICountryService  {

    private CountryRepository repository;


    @Override
    public Country save(Country country) {
        return repository.save(country);
    }

    @Override
    public Country update(Country country) {
        Country updateCountery= findById(country.getId());
        updateCountery.setName(country.getName());
        return repository.save(country);
    }

    @Override
    public void delete(Long id) {
        findById(id);
        repository.deleteById(id);
    }

    @Override
    public Country findById(Long id) {
        Optional<Country> optionalCountry=repository.findById(id);
        if (!optionalCountry.isPresent()){
            throw new NotFoundException("Not Found Country");
        }
        return optionalCountry.get();
    }

    @Override
    public Page<Country> findAll(Integer page, Integer size) {
        return repository.findAll(PageRequest.of( page, size));
    }

}
