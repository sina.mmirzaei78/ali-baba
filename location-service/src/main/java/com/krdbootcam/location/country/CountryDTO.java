package com.krdbootcam.location.country;

import com.krdbootcam.location.common.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CountryDTO  extends BaseDTO {

    @ApiModelProperty(required = true,hidden = false)
    private  String name;
}
