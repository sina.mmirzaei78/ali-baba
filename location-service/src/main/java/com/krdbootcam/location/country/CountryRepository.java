package com.krdbootcam.location.country;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface CountryRepository extends PagingAndSortingRepository<Country,Long> {
   Page<Country>findAll(Pageable pageable);

}
