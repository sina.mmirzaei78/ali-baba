package com.krdbootcam.location.country;


import com.krdbootcam.location.common.PagingData;
import lombok.AllArgsConstructor;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometries;
import org.geolatte.geom.Point;
import org.geolatte.geom.crs.CoordinateReferenceSystems;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api-country/v1")
@AllArgsConstructor
public class CountryController {

    private final ICountryService iCountryService;
    private final CountryMapper mapper;

    @PostMapping
    public ResponseEntity save(@RequestBody CountryDTO countryDTO){
        Country country=mapper.toCountry(countryDTO);
        iCountryService.save(country);
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @PutMapping
    public ResponseEntity update(@RequestBody CountryDTO countryDTO){
        Country country=mapper.toCountry(countryDTO);
        iCountryService.save(country);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        iCountryService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public  ResponseEntity findById(@PathVariable Long id){
        Country country=iCountryService.findById(id);
        CountryDTO countryDTO=mapper.toCountryDTO(country);
        return ResponseEntity.ok(countryDTO);
    }

    @GetMapping("/{page}/{size}")
    public ResponseEntity<PagingData<CountryDTO>> findAll(@PathVariable Integer page,@PathVariable Integer size){
        Page<Country> countryPage=iCountryService.findAll(page,size);
        int total=countryPage.getTotalPages();
        List<Country> countries=countryPage.getContent();
        List<CountryDTO> countryDTOS=mapper.toCountryDTOs(countries);
        PagingData<CountryDTO> countryDTOPagingData=new PagingData<>(total,page,countryDTOS);
        return ResponseEntity.ok(countryDTOPagingData);
    }





}

