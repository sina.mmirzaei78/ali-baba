package com.krdbootcam.location.country;

import com.krdbootcam.location.common.BaseEntity;
import com.krdbootcam.location.province.Province;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Audited
@Table(name= "tbl_country")
@Data
public class Country extends BaseEntity {

    @NotNull
    @Column(name = "name")
    private  String name;

    @OneToMany(mappedBy = "country", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Province> provinces;
}
