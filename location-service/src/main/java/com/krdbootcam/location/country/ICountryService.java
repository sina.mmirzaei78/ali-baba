package com.krdbootcam.location.country;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface ICountryService {

    Country save(Country country);
    Country update(Country country);
    void delete(Long id);
    Country findById(Long id);
    Page<Country> findAll(Integer page,Integer size );

}
