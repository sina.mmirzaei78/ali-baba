package com.krdbootcam.location.country;

import java.util.List;

public interface CountryMapper {

    Country toCountry(CountryDTO countryDTO);
    CountryDTO toCountryDTO(Country country);
    List<Country> toCountryList(List<CountryDTO> countryDTOS);
    List<CountryDTO> toCountryDTOs(List<Country> countryList);
}
