package com.krdbootcam.location.city;


import com.krdbootcam.location.common.BaseEntity;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Data
@Entity
@Audited
@Table(name = "tbl_city")
public class City extends BaseEntity {

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "proved_id")
    private Long provedId;
}
