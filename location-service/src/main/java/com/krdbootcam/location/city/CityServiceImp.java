package com.krdbootcam.location.city;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CityServiceImp implements CityService {

    private final CityRepository repository;

    @Override
    public City save(City city) {
        return null;
    }

    @Override
    public City update(City city) {

        City saveCity=getById(city.getId());
        saveCity.setName(city.getName());
        return null;
    }

    @Override
    public void delete(Long id) {
    getById(id);
    repository.deleteById(id);
    }

    @Override
    public City getById(Long id) {
        Optional<City> optionalCity=repository.findById(id);
        if (!optionalCity.isPresent()){
            throw new RuntimeException("Not found");
        }
        return optionalCity.get();
    }
}
