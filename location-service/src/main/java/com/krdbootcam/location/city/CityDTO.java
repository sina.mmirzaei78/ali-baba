package com.krdbootcam.location.city;

import com.krdbootcam.location.common.BaseDTO;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

@Data
public class CityDTO extends BaseDTO {

    @ApiModelProperty(required = true, hidden = false)
    private String name;

    @ApiModelProperty(required = true, hidden = false)
    private Long proveiId;

}