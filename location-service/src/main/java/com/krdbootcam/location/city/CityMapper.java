package com.krdbootcam.location.city;

import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CityMapper {

    City toCity(CityDTO cityDTO);
    CityDTO toCityDTO(City city);

    List<City> toListCity(List<CityDTO> cityDTOS);
    List<CityDTO> toListCityDTO(List<City> cities);

}
