package com.krdbootcam.location.city;


public interface CityService {

    City save(City city);
    City update(City city);
    void delete(Long id);
    City getById(Long id);


}
